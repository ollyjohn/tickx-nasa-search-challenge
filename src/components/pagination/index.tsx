import React from "react";

import classnames from "classnames";

import { Button, ButtonGroup } from "@mui/material";
import { ArrowBack, ArrowForward } from "@mui/icons-material";

import styles from "./styles.module.css";

const Pagination = (props: { page: number, changePage: Function }) => (
  <ButtonGroup className={styles.buttonGroup}>
    <Button
      className={styles.paginationButton}
      disabled={props.page === 1}
      onClick={() => props.changePage(props.page - 1)}
      variant="contained"
      style={{
        borderTopLeftRadius: "20px",
        borderBottomLeftRadius: "20px",
        width: "30px"
      }}
    >
      <ArrowBack />
    </Button>
    <Button className={classnames(styles.paginationButton, styles.pageLabel)} variant="contained">
      Page {props.page}
    </Button>
    <Button
      className={styles.paginationButton}
      onClick={() => props.changePage(props.page + 1)}
      variant="contained"
      style={{
        borderTopRightRadius: "20px",
        borderBottomRightRadius: "20px",
        width: "30px"
      }}
    >
      <ArrowForward />
    </Button>
  </ButtonGroup>
);

export default Pagination;