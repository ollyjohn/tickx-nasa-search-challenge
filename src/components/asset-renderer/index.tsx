import React, { useMemo, useState } from "react";

import { Skeleton } from "@mui/material";

import useWindowSize from "../../hooks/use-window-size";

import styles from "./styles.module.css";

const AssetRenderer = (props: { mediaType?: string | null, src?: string, imageAlt?: string }) => {
  const [loaded, setLoaded] = useState<Boolean>(false);
  const { height, width } = useWindowSize();

  // max image height is 60vh, so take the window height * 0.6 & use that for placeholder
  // while the image loads so the content isn't jumping around.
  const placeholderHeight = useMemo(() => Math.floor(height * 0.6), [height]);
  // make media player take up 60% (or 75% when width < 900px) of screen width - have to provide
  // absolute pixel values so need to make use of the useWindowSize hook
  const mediaPlayerWidth = useMemo(() => Math.floor(width * (width > 900 ? 0.6 : 0.75)), [width]);

  return (
    <div className={styles.mediaWrapper}>
      { props.mediaType && props.src && props.mediaType === "audio" && <audio controls src={props.src} className={styles.mediaPlayer} />}
      { props.mediaType && props.src && props.mediaType === "image" && 
        <img className={styles.image} src={props.src} style={loaded ? {} : { display: "none" }} alt={props.imageAlt} onLoad={() => setLoaded(true)} />
      }
      { props.mediaType === "image" && !loaded && <Skeleton animation="wave" height={placeholderHeight} width={400} variant="rectangular" />}
      { props.mediaType && props.src && props.mediaType === "video" && <video src={props.src} width={mediaPlayerWidth} controls />}
    </div>
  );
};

export default AssetRenderer;