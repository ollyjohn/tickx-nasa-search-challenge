import React, { useMemo } from "react";

import { ImageList, Skeleton } from "@mui/material";

import Asset from "../../interfaces/asset.interface";

import useWindowSize from "../../hooks/use-window-size";

import GalleryItem from "../gallery-item";

const BREAKPOINTS = [0, 600, 900, 1200, 1500];
const PLACEHOLDER = Array(25).fill(undefined);

const Gallery = (props: { items: Asset[], isLoading: Boolean }) => {
  const { width: windowWidth } = useWindowSize();

  const cols = useMemo(() => BREAKPOINTS.filter((w: number) => windowWidth >= w).length, [windowWidth]);

  return (
    <ImageList variant="quilted" cols={cols} rowHeight={250} gap={10} style={{ padding: "20px 20px" }}>
      {props.isLoading
        ? PLACEHOLDER.map((el, i) => (
          <Skeleton variant="rectangular" width="100%" height={250} animation="wave" key={i} />)
        )
        : props.items.map((item: Asset) => (
          <GalleryItem item={item} key={item.data[0].nasa_id} />
        )
      )}
    </ImageList>
  );
}

export default Gallery;
