import React, { useMemo, useState } from "react";

import { Button, Typography } from "@mui/material";

import styles from "./styles.module.css";

const TruncatableText = (props: { text: string, maxLength: number }) => {
  const [showFull, setShowFull] = useState(false);

  const toggleable = props.text.length > props.maxLength;

  const visibleString = useMemo(() => 
    props.text.length > props.maxLength && !showFull ? props.text.slice(0, props.maxLength) : props.text,
    [props.text, props.maxLength, showFull]
  );

  function toggleShowFull() {
    setShowFull(!showFull);
  }

  return (
    <div className={styles.wrapper}>
      <Typography variant="body1" width="100%" className={styles.text}>
        {visibleString}
        {toggleable && !showFull && "..."}
      </Typography>
      {toggleable && <Button variant="contained" onClick={toggleShowFull} className={styles.toggler}>Show {showFull ? "less" : "more"}</Button>}
    </div>
  )
};

export default TruncatableText;