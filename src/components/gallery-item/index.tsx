import React, { useMemo } from "react";
import { useNavigate } from "react-router-dom";

import { Box, ImageListItem, ImageListItemBar, Tooltip } from "@mui/material";
import { VolumeUpRounded } from "@mui/icons-material"

import Asset from "../../interfaces/asset.interface";

import styles from "./styles.module.css";

const ACTIONS = {
  image: "view",
  audio: "listen",
  video: "watch"
};

const GalleryItem = (props: { item: Asset }) => {
  const navigate = useNavigate();

  const data = useMemo(() => props.item.data[0], [props.item]);
  const thumbnail = useMemo(() => 
    ["image", "video"].includes(data.media_type)
      ? (props.item.links || []).find(({ rel }) => rel === "preview")?.href
      : "" as string,
    [data, props.item.links]
  );

  return (
    <Tooltip arrow title={data.title} followCursor disableHoverListener={data.media_type === "image"}>
      <ImageListItem classes={{ root: styles.wrapper }} onClick={() => navigate(`/asset/${data.nasa_id}?type=${data.media_type}`)}>
        {thumbnail
          ? <img className={styles.item } src={thumbnail} alt={data.title} loading="lazy" />
          : <Box className={styles.box}><VolumeUpRounded className={styles.icon} /></Box>
        }

        <ImageListItemBar title={data.title} subtitle={`Click to ${ACTIONS[data.media_type]}`} />
      </ImageListItem>
    </Tooltip>
  )
}

export default GalleryItem;
