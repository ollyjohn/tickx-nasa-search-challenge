import React, { useEffect, useState, useMemo, useRef, Fragment } from "react";
import { useParams, useSearchParams, useNavigate } from "react-router-dom";

import { Alert, Button, Container, Grid, Paper, Skeleton, Snackbar, Typography } from "@mui/material";
import { ArrowBack } from "@mui/icons-material";

import AssetMetadata from "../../interfaces/asset-metadata.interface";

import useAssetMetadata from "../../hooks/use-metadata";
import useAsset from "../../hooks/use-asset";

import AssetRenderer from "../../components/asset-renderer";
import TruncatableText from "../../components/truncatable-text";

import styles from "./styles.module.css";
import "../../App.css";

const AssetView = () => {
  const [searchParams] = useSearchParams();
  const mediaType = searchParams.get("type"); // "audio" | "image" | "video" - used to determine render frame

  const params = useParams();
  const navigate = useNavigate();
  const getMetadata = useAssetMetadata(mediaType || "", params.id || "");
  const getAsset = useAsset(params.id || "", mediaType || "");

  const firstRender = useRef(true);

  const [assetSrc, setAssetSrc] = useState<string>();
  const [metadata, setMetadata] = useState<AssetMetadata>();
  const [error, setError] = useState<string>();

  const doRender = useMemo(() => mediaType && assetSrc && metadata, [assetSrc, mediaType, metadata]);

  useEffect(() => {
    if (firstRender.current) {
      firstRender.current = false;
      getMetadata()
        .then(m => {
          getAsset()
            .then(a => {
              setMetadata(m);
              setAssetSrc(a);
            })
            .catch(error => {
              console.error(error);
              setError("Failed to fetch asset URL from NASA")
            })
        })
        .catch(error => {
          console.error(error);
          setError("Failed to fetch asset metadata from NASA")
        });
    }
  });
  useEffect(() => {
    document.title = `${document.title.split("|")[0]} | Asset ${params.id}`
  }, [params.id]);


  return (
    <Fragment>
      <Container>
        <Grid container item xs={12} my="2.5vh" spacing={3} justifyContent="center">
          <Paper elevation={3} className={styles.assetWrapper}>
            <Grid container item xs={12} spacing={2} justifyContent="center">
              <Grid item xs={12}>
                <Button variant="contained" onClick={() => navigate("/search")} startIcon={<ArrowBack />}>Back to search</Button>
              </Grid>
              <Grid item xs={12}>
                <Typography variant="h1" className="pageTitle">
                  {doRender ? metadata && (metadata["XMP:Title"] || metadata["AVAIL:Title"]) : <Skeleton animation="wave" />}
                </Typography>
              </Grid>
            </Grid>
            <Grid container item xs={12} alignItems="center" justifyContent="center" mt={5}>
              {doRender
                ? <AssetRenderer mediaType={mediaType} src={assetSrc} />
                : <Skeleton animation="wave" variant="rectangular" width="100%" height={400} />
              }
            </Grid>
            <Grid item xs={12} mt={5}>
              {doRender ? metadata && <TruncatableText text={(metadata["XMP:Description"] || metadata["AVAIL:Description"])} maxLength={1000} /> : (
                <Fragment>
                  <Skeleton animation="wave" />
                  <Skeleton animation="wave" />
                  <Skeleton animation="wave" />
                </Fragment>
              )}
            </Grid>
          </Paper>
        </Grid>
      </Container>
      <Snackbar open={!!error} onClose={() => setError("")} autoHideDuration={10000}>
        <Alert elevation={5} severity="error">{error}</Alert>
      </Snackbar>
    </Fragment>
  );
};

export default AssetView;
