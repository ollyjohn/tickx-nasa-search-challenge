import React, { ChangeEvent, useEffect, useMemo, useRef, useState } from "react";
import { useSearchParams } from "react-router-dom";

import usePersistentState from "use-local-storage-state";

import { Button, Container, FormControl, FormLabel, FormGroup, FormControlLabel, Grid, InputAdornment, TextField, Typography, Checkbox } from "@mui/material";
import { Search as SearchIcon } from "@mui/icons-material"

import Asset from "../../interfaces/asset.interface";

import useAssets from "../../hooks/use-assets";

import Gallery from "../../components/gallery";
import Pagination from "../../components/pagination";

import "../../App.css"; // global styles
import styles from "./styles.module.css";

const Search = () => {
  const [params, setParams] = useSearchParams();

  const renderRef = useRef(true);

  const [isLoading, setIsLoading] = useState<Boolean>(true);
  const [assets, setAssets] = useState<Asset[]>([]);
  const [query, setQuery] = usePersistentState<string>("query", "");
  const [mediaTypes, setMediaTypes] = usePersistentState("mediaTypes", [
    { name: "audio", label: "Audio", checked: true }, 
    { name: "image", label: "Image", checked: true },
    { name: "video", label: "Video", checked: true }
  ]);

  const page = useMemo(() => {
    const param: string | null = params.get("page");
    if (param) {
      return +param;
    } else {
      return 1;
    }
  }, [params]);
  const canSearch = useMemo(() => !!Object.values(mediaTypes).filter(t => t).length || !!query.length, [mediaTypes, query]);

  const getAssets = useAssets(
    mediaTypes.filter(({ checked }) => checked).map(({ name }) => name).join(","),
    query,
    page
  );

  const submitSearch = async () => {
    setIsLoading(true);

    try {
      const data = await getAssets();
      setIsLoading(false);
      setAssets(data);
    } catch (error) {
      console.error(error);
      setIsLoading(false);
    };
  };

  useEffect(() => {
    // do an initial fetch of the images if this is the first render cycle
    if (renderRef.current) {
      renderRef.current = false;
      submitSearch();
    }
  });
  useEffect(() => {
    submitSearch();
  }, [page, mediaTypes]); // eslint-disable-line
  useEffect(() => {
    document.title = `${document.title.split("|")[0]} | Search results${query && ` for "${query}"`} - page ${page}`
  }, [query, page]);

  const changePage = (newPage: number) => setParams({ page: newPage.toString() });
  const handleMediaTypeChange = ({ target: { name, checked }}: ChangeEvent<HTMLInputElement>) => 
    setMediaTypes(mediaTypes.map(t => t.name === name ? ({ ...t, checked }) : t)
  );

  return (
    <Container maxWidth="xl">
      <Grid container spacing={4} alignItems="center" justifyContent="center">
        <Grid item xs={12} mt="5vh">
          <Typography variant="h1" className="pageTitle">NASA Search</Typography>
          <Typography variant="subtitle1" align="center">
            Search for assets by keyword, or go through the gallery below &amp; click on assets to view their details
          </Typography>
        </Grid>
        <Grid container item xs={12} lg={6} justifyContent="center" spacing={2}>
          <Grid item xs={12}>
            <TextField
              id="search-box"
              variant="outlined"
              fullWidth
              value={query}
              placeholder="Search"
              onChange={e => setQuery(e.target.value)}
              InputProps={{
                classes: {
                  root: styles.searchBoxRoot
                },
                endAdornment: (
                  <InputAdornment position="end">
                    <Button
                      color="primary"
                      disabled={!canSearch}
                      variant="contained"
                      onClick={() => submitSearch()}
                      endIcon={<SearchIcon />}
                      className={styles.searchButton}
                    >
                      Search
                    </Button>
                  </InputAdornment>
                )
              }}
            />
          </Grid>
          <Grid container item xs={12} justifyContent="center">
            <FormControl component="fieldset" variant="filled" fullWidth>
              <FormLabel component="legend">Media type(s)</FormLabel>
              <FormGroup row className={styles.checkGroup}>
                {mediaTypes.map(t => (
                  <FormControlLabel key={t.name} control={<Checkbox checked={t.checked} name={t.name} onChange={handleMediaTypeChange} />} label={t.label} />
                ))}
              </FormGroup>
            </FormControl>

          </Grid>
        </Grid>
      </Grid>
      <Grid container justifyContent="center" spacing={2} mt={5} mb={5}>
        <Pagination page={page} changePage={changePage} />
        <Grid item xs={12}><Gallery items={assets} isLoading={isLoading} /></Grid>
        <Pagination page={page} changePage={changePage} />
      </Grid>
    </Container>
  );
}

export default Search;