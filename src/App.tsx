import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import Search from "./views/search";
import Asset from './views/asset';

import './App.css';

const App = () => (
  <div className="app">
    <BrowserRouter>
      <Routes>
        <Route key="search" path="/search"  element={<Search />} />
        <Route key="asset" path="/asset/:id"  element={<Asset />} />
        <Route key="fallback" path="*" element={<Search />} />
      </Routes>
    </BrowserRouter>
  </div>
);

export default App;
