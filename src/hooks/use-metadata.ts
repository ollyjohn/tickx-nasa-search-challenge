import { useCallback } from "react";

import { getAssetMetadata } from "../apis/nasa.api";

const useAssetMetadata = (mediaType: string, assetId: string) => useCallback(async () => {
  return await getAssetMetadata(mediaType, assetId);
}, [mediaType, assetId])

export default useAssetMetadata;
