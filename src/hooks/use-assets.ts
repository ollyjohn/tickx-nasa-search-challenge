import { useCallback } from "react";

import { getMediaItems } from "../apis/nasa.api";

const useAssets = (mediaTypes: string, query?: string, page?: number) => useCallback(async () => {
  return await getMediaItems(mediaTypes, query, page);
}, [mediaTypes, query, page])

export default useAssets;
