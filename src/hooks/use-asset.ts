import { useCallback } from "react";

import { getAsset } from "../apis/nasa.api";

const useAsset = (id: string, mediaType: string) => useCallback(async () => {
  return await getAsset(id, mediaType);
}, [id, mediaType])

export default useAsset;
