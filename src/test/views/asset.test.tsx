import React from "react";

import { cleanup, render,  waitFor , screen, fireEvent} from "@testing-library/react";
import { act } from "react-dom/test-utils";

import axios from "axios";
import MockAdapter from "axios-mock-adapter";

import { asset, assetMetadata } from "../fixtures/data";
import { sleep } from "../fixtures/util";

import Asset from "../../views/asset";
import AssetStyles from "../../views/asset/styles.module.css";

const mockedSetParams = jest.fn();
const mockedNavigate = jest.fn();

jest.mock("react-router-dom", () => ({
  useNavigate: () => mockedNavigate,
  useParams: () => ({ id: "TEST" }),
  useSearchParams: () => [
    { get: jest.fn(() => "image") },
    mockedSetParams
  ]
}));
const axiosMock = new MockAdapter(axios, { delayResponse: 500 });

jest.setTimeout(10000); // just in case...
describe("<Asset ... />", () => {

  beforeAll(() => {
    axiosMock
      .onGet("https://images-api.nasa.gov/asset/TEST")
        .reply(200, asset)
      .onGet("https://images-assets.nasa.gov/image/TEST/metadata.json")
        .reply(200, assetMetadata);
  });
  afterAll(() => jest.clearAllMocks());
  afterEach(cleanup);

  it("Should mount okay", async () => {

    await act(async () => {
      const { container } = render(<Asset />);

      await waitFor(() => expect(container.querySelector(".MuiSkeleton-root")).toBeInTheDocument());

      // while the asset URL & metadata are loading, we should see 5 skeleton placeholders
      expect(container.querySelectorAll(".MuiSkeleton-root")).toHaveLength(5)

      // sleep long enough to make sure the data's rendered
      await sleep(2000);

      expect(container.querySelector(".pageTitle")).toHaveTextContent("Test asset title");
      expect(container.querySelector("p")).toHaveTextContent("Test asset description text")
    });
  });

  it("Should navigate to /search when the back button is clicked", async () => {
    const { getByRole } = render(<Asset />);

    await waitFor(() => expect(getByRole("button")).toBeInTheDocument());

    act(() => {
      const button = getByRole("button");

      fireEvent.click(button);
    });

    expect(mockedNavigate).toHaveBeenCalledWith("/search");
  });

});
