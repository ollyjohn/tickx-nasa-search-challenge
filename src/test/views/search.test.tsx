import React from "react";

import { cleanup, render,  waitFor , screen, fireEvent} from "@testing-library/react";
import { act } from "react-dom/test-utils";

import axios from "axios";
import MockAdapter from "axios-mock-adapter";

import Search from "../../views/search";
import { mockedAssetsWrapped } from "../fixtures/data";

const mockedSetParams = jest.fn();

jest.mock("react-router-dom", () => ({
  useNavigate: () => jest.fn(),
  useSearchParams: () => [
    { get: jest.fn(() => 1) },
    mockedSetParams
  ]
}));
const axiosMock = new MockAdapter(axios);

describe("<Search ... />", () => {

  beforeAll(() => {
    axiosMock.onGet(/https\:\/\/images\-api\.nasa\.gov\/search\?.*/)
      .reply(200, mockedAssetsWrapped);
  });
  afterAll(() => jest.clearAllMocks());
  afterEach(() => {
    cleanup();
    localStorage.clear();
  });

  it("Should mount okay", async () => {
    const { container, getByText, getByPlaceholderText } = render(<Search />);

    // ensure page has loaded & heading is correct
    await waitFor(() => expect(getByText("NASA Search")).toBeInTheDocument());
    // ensure localStorage has been queried for page & query
    expect(localStorage.getItem).toHaveBeenCalled();
    // ensure search bar rendered
    expect(getByPlaceholderText("Search")).toBeInTheDocument();
    // ensure pagination rendered & shows the correct page
    expect((container.querySelector(".MuiButtonGroup-root") as HTMLElement).children[1].textContent).toEqual("Page 1");
    // ensure all assets from mocked response are present
    expect((container.querySelector(".MuiImageList-root") as HTMLElement).children).toHaveLength(3);
    // ensure the appropriate REST call has been made
    expect(axiosMock.history.get[0].url).toEqual("https://images-api.nasa.gov/search?media_type=audio%2Cimage%2Cvideo&page=1");
  });

  it("Should have document.title set to reflect the current page", async () => {
    const { getByText } = render(<Search />);

    await waitFor(() => expect(getByText("NASA Search")).toBeInTheDocument());

    expect(document.title.includes("page 1")).toBeTruthy();
  });

  it("Should update localStorage when the user changes the query", async () => {
    const { getByPlaceholderText, getByText } = render(<Search />);

    await waitFor(() => expect(getByText("NASA Search")).toBeInTheDocument());

    act(() => {
      const searchField = getByPlaceholderText("Search");
      fireEvent.change(searchField, { target: { value: "Test" } });
    });

    expect(localStorage.setItem).toHaveBeenLastCalledWith("query", "\"Test\"");
  });

  it("Should update localStorage when the user changes their media type selections", async () => {
    const { container, getByText } = render(<Search />);

    await waitFor(() => expect(getByText("NASA Search")).toBeInTheDocument());

    act(() => {
      const checkbox = container.querySelector("input[type='checkbox']") as HTMLElement;
      fireEvent.click(checkbox);
    });

    expect(localStorage.setItem).toHaveBeenLastCalledWith(
      "mediaTypes",
      "[{\"name\":\"audio\",\"label\":\"Audio\",\"checked\":false},{\"name\":\"image\",\"label\":\"Image\",\"checked\":true},{\"name\":\"video\",\"label\":\"Video\",\"checked\":true}]"
    );
  })

  it("Should update localStorage & document title when the user changes page", async () => {
    const { container, getByText } = render(<Search />);

    await waitFor(() => expect(getByText("NASA Search")).toBeInTheDocument());

    act(() => {
      const button = (container.querySelector(".MuiButtonGroup-root") as HTMLElement).children[2];

      fireEvent.click(button);
    });

    expect(mockedSetParams).toHaveBeenLastCalledWith({ page: "2" });
  });
});