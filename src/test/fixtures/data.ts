import Asset from "../../interfaces/asset.interface";
import AssetMetadata from "../../interfaces/asset-metadata.interface";


export const mockedAssets: Asset[] = [
  {
    "href": "https://images-assets.nasa.gov/video/KSC-19640101-MH-NAS01-0001-The_Crawler_Transporter_The_Beginning_Historical_Footage-B_2309/collection.json",
    "data": [
      {
        "description": "Crawler-Transporter footage including scenes from the Marion Power Shovel Company in Marion, Ohio and subsequent footage at KSC on the crawlerway, and transporting an Apollo/Saturn vehicle near the VAB. Total run time: 00:10:28",
        "title": "Crawler-Transporter footage",
        "photographer": "NASA",
        "nasa_id": "KSC-19640101-MH-NAS01-0001-The_Crawler_Transporter_The_Beginning_Historical_Footage-B_2309",
        "date_created": "1964-01-01T00:00:00Z",
        "keywords": [
          "crawler-transporter",
          "Apollo",
          "Marion Power Shovel Company"
        ],
        "media_type": "video",
        "center": "KSC"
      }
    ],
    "links": [
      {
        "href": "https://picsum.photos/500",
        "rel": "preview",
        "render": "image"
      },
      {
        "href": "https://images-assets.nasa.gov/video/KSC-19640101-MH-NAS01-0001-The_Crawler_Transporter_The_Beginning_Historical_Footage-B_2309/KSC-19640101-MH-NAS01-0001-The_Crawler_Transporter_The_Beginning_Historical_Footage-B_2309.vtt",
        "rel": "captions"
      }
    ]
  },
  {
    "href": "https://images-assets.nasa.gov/audio/KSC-20190521-PC-MMS01-0001_RocketRanch10_3218706/collection.json",
    "data": [
      {
        "description": "NASA’s latest exploration goals center on returning humans to the Moon – not just for a visit, but to stay. At the center of that plan is Gateway. It’s a small lunar outpost that will have living quarters, laboratories for science and research, docking ports for visiting spacecraft, and more.",
        "title": "Rocket Ranch Podcast Episode 10: Gateway",
        "photographer": "NASA/Michelle Stone",
        "location": "Web Studio",
        "nasa_id": "KSC-20190521-PC-MMS01-0001_RocketRanch10_3218706",
        "date_created": "2019-05-21T00:00:00Z",
        "keywords": [
          "Rocket Ranch",
          "podcast",
          "moon",
          "nasa",
          "gateway"
        ],
        "media_type": "audio",
        "center": "KSC"
      }
    ]
  },
  {
    "href": "https://images-assets.nasa.gov/image/KSC-07pd0009/collection.json",
    "data": [
      {
        "center": "KSC",
        "title": "KSC-07pd0009",
        "location": "Kennedy Space Center, FL",
        "nasa_id": "KSC-07pd0009",
        "date_created": "2007-01-04T00:00:00Z",
        "media_type": "image",
        "description": "KENNEDY SPACE CENTER, FLA. --   Endeavour's payload bay is open for payload configuration work in Orbiter Processing Facility bay 2.  The orbiter is the vehicle designated for mission STS-118, scheduled to launch in late June. The mission will continue space station construction by delivering a third starboard truss segment, S5, as well as carrying the External Stowage Platform 3. The crew comprises six astronauts: Commander Scott Kelly, Pilot Charles Hobaugh and Mission Specialists Dr. Dafydd (Dave) Williams, Barbara Morgan, Richard Mastracchio and Tracy Caldwell.  Williams represents the Canadian Space Agency.  Photo credit: NASA/George Shelton"
      }
    ],
    "links": [
      {
        "href": "https://picsum.photos/500",
        "rel": "preview",
        "render": "image"
      }
    ]
  }
];

export const mockedAssetsWrapped = {
  "collection": {
    "items": mockedAssets
  }
}

export const asset = { collection: { items: [{ href: "https://i.picsum.photos/id/408/2000/1000.jpg?hmac=dLJi6k6lhFiNeBLSszaYqNmVz95ZkIJTpSvn-gVWAs8" }] } };

export const assetMetadata: AssetMetadata = {
  "AVAIL:Description": "Test asset description text",
  "AVAIL:Title": "Test asset title",
  "XMP:Description": "Test asset description text",
  "XMP:Title": "Test asset title"
};
