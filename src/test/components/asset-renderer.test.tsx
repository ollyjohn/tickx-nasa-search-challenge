import React from "react";

import { render } from "@testing-library/react";


import AssetRenderer from "../../components/asset-renderer";

describe("<AssetRenderer ... />", () => {
  it("Should render an <audio /> element for audio assets", () => {
    const { container } = render(<AssetRenderer mediaType="audio" src="https://www.soundboard.com/mediafiles/mz/Mzg1ODMxNTIzMzg1ODM3_JzthsfvUY24.mp3" />);

    const elm = container.querySelector("audio");

    expect(elm).toBeInTheDocument();
  });
  it("Should render an <img /> element & skeleton placeholder for image assets", () => {
    const { container } = render(<AssetRenderer mediaType="image" src="https://picsum.photos/2000/1000" imageAlt="A placeholder image" />);

    const placeholder = container.querySelector(".MuiSkeleton-root");

    expect(placeholder).toBeInTheDocument();

    const image = container.querySelector("img");

    expect(image).toBeInTheDocument();
  });
  it("Should render a <video /> element for video assets", () => {
    const { container } = render(<AssetRenderer mediaType="video" src="http://images-assets.nasa.gov/video/SSC_2016_2016-08-18 - RS-25 Hot Fire test/SSC_2016_2016-08-18 - RS-25 Hot Fire test~small.mp4" />);

    const elm = container.querySelector("video");

    expect(elm).toBeInTheDocument();
  });
});