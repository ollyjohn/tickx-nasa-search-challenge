import React from "react";

import { render, screen } from "@testing-library/react";
import TruncatableText from "../../components/truncatable-text";

describe("<TruncatableText ... />", () => {
  const okayString = "Hello!";
  const longString = "This is a long string.  It should be truncated.";
  const trncLength = 20;


  it("Shouldn't render the truncation ellipsis or button if the string is shorter than the limit", () => {
    render(<TruncatableText text={okayString} maxLength={trncLength} />);

    const text = screen.getByText("Hello!");

    expect(text).toBeInTheDocument();

    try {
      screen.getByRole("button");
    } catch (error) {
      const cast = error as Error;
      expect(cast.message.includes("Unable to find an accessible element with the role \"button\"")).toBeTruthy();
    }
  });

  it("Should render the ellipsis & \"Show more\" button if the string is longer than the limit", () => {
    const { container } = render(<TruncatableText text={longString} maxLength={trncLength} />);

    const txt = container.querySelector(".text");
    const btn = container.querySelector(".toggler");

    expect(txt).toBeInTheDocument();
    expect(txt).toHaveTextContent("...");
    expect(btn).toBeInTheDocument();
    expect(btn).toHaveTextContent("Show more");
  });

  it("Should render the \"Show less\" button if the string is longer than the limit and already expanded", () => {
    render(<TruncatableText text={longString} maxLength={trncLength} />);

    const btn = screen.getByRole("button");

    expect(btn).toBeInTheDocument();
    
    btn.click();
    
    expect(btn).toHaveTextContent("Show less");
  });
  
  it("Should toggle the button text depending on whether text is truncated/expanded", () => {
    render(<TruncatableText text={longString} maxLength={trncLength} />);
  
    const btn = screen.getByRole("button");
  
    expect(btn).toBeInTheDocument();
    
    expect(btn).toHaveTextContent("Show more");

    btn.click();

    expect(btn).toHaveTextContent("Show less");

    btn.click();

    expect(btn).toHaveTextContent("Show more");
  });
});