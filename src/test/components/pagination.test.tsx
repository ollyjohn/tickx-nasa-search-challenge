import React from "react";

import { render, screen } from "@testing-library/react";

import Pagination from "../../components/pagination";

const changePage = jest.fn();

describe("<Pagination ... />", () => {

  it("Should render the current page correctly", () => {
    render(<Pagination page={1} changePage={changePage} />);

    const pageNo = screen.getByText("Page 1");

    expect(pageNo).toBeInTheDocument();
  });

  it("Should not be able to decrement the page if page === 1", () => {
    render(<Pagination page={1} changePage={changePage} />);

    const firstButton = screen.getAllByRole("button")[0];

    expect(firstButton).toHaveClass("Mui-disabled");  // implies button is disabled
  });

  it("Should be able to decrement the page if page > 1", () => {
    render(<Pagination page={2} changePage={changePage} />);

    const firstButton = screen.getAllByRole("button")[0];

    expect(firstButton).not.toHaveClass("Mui-disabled");
  });

  it("Should call changePage correctly when decrementing", () => {
    render(<Pagination page={2} changePage={changePage} />);

    const firstButton = screen.getAllByRole("button")[0];

    firstButton.click();

    expect(changePage).toHaveBeenCalledWith(1);
  });

  it("Should call changePage correctly when incrementing", () => {
    render(<Pagination page={2} changePage={changePage} />);

    const lastButton = screen.getAllByRole("button")[2]; // should only ever be 3 buttons in this component...

    lastButton.click();

    expect(changePage).toHaveBeenCalledWith(3);
  });
})