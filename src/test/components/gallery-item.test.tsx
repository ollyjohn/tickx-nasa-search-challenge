import React from "react";

import { render, screen } from "@testing-library/react";

import Asset from "../../interfaces/asset.interface";

import GalleryItem from "../../components/gallery-item";

import { mockedAssets } from "../fixtures/data";

const mockedNavigate = jest.fn();
jest.mock("react-router-dom", () => ({
  useNavigate: () => mockedNavigate
}));

describe("<GalleryItem ... />", () => {
  
  it("Clicking on an item should call navigate()", () => {
    render(<GalleryItem item={mockedAssets[0]} />);
    
    const assets = screen.getAllByLabelText("Crawler-Transporter footage")

    assets[0].click();

    expect(mockedNavigate).toHaveBeenCalledWith("/asset/KSC-19640101-MH-NAS01-0001-The_Crawler_Transporter_The_Beginning_Historical_Footage-B_2309?type=video")
  });

  it("Audio assets should get a pre-baked graphic thumbnail", () => {
    const { container } = render(<GalleryItem item={mockedAssets[1]} />);

    const asset = container.querySelector(".MuiImageListItem-root") as HTMLElement;

    expect(asset.children[0].children[0].nodeName).toEqual("svg");  // who needs consistent casing?
  });
  it("Images assets should get a thumbnail image", () => {
    const { container } = render(<GalleryItem item={mockedAssets[2]} />);

    const asset = container.querySelector(".MuiImageListItem-root") as HTMLElement;

    expect(asset.children[0].nodeName).toEqual("IMG");
  });
  it("Video assets should get a thumbnail image", () => {
    const { container } = render(<GalleryItem item={mockedAssets[0]} />);

    const asset = container.querySelector(".MuiImageListItem-root") as HTMLElement;

    expect(asset.children[0].nodeName).toEqual("IMG");
  });
});