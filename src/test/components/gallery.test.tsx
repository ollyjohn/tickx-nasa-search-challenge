import React from "react";
import { Router } from "react-router";

import { render } from "@testing-library/react";

import Gallery from "../../components/gallery";

import { mockedAssets } from "../fixtures/data";

const mockedNavigate = jest.fn();
jest.mock("react-router-dom", () => ({
  useNavigate: () => mockedNavigate
}));

describe("<Gallery />", () => {


  it("Should render 25 placeholder items while the data is loading", () => {
    const { container } = render(<Gallery isLoading items={[]} />);

    const placeholders = container.querySelectorAll(".MuiSkeleton-rectangular");

    expect(placeholders).toHaveLength(25);
  });

  it("Should render as many items as it receives", () => {
    const { container } = render(<Gallery items={mockedAssets} isLoading={false} />);

    const assets = container.querySelectorAll(".MuiImageListItem-root");
    
    expect(assets.length).toEqual(mockedAssets.length);
  });
});