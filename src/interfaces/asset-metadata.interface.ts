export default interface AssetMetadata {
  'AVAIL:Title': string;
  'AVAIL:Description': string;
  'XMP:Title': string;
  'XMP:Description': string;
}
