export default interface Asset {
  href: string;
  data: {
    center: string;
    title: string;
    photographer?: string;
    location?: string;
    nasa_id: string;
    date_created: string;
    media_type: "image" | "audio" | "video";
    description: string;
    keywords?: string[];
  }[];
  links?: {
    href: string;
    rel: string;
    render?: string;
  }[];
}