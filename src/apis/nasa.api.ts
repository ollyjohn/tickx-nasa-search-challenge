import axios from "axios";

import AssetMetadata from "../interfaces/asset-metadata.interface";
import Asset from "../interfaces/asset.interface";

const BASE_URL = "https://images-api.nasa.gov"


/**
 * Query the NASA images API to get a list of matching images/audio/video(?) files
 * @param query Query to apply to the search
 * @returns A list of matching media items
 */
export async function getMediaItems(mediaTypes: string, query?: string, page?: number): Promise<Asset[]> {
  
  const { data } = await axios.get(`${BASE_URL}/search?media_type=${encodeURIComponent(mediaTypes)}${query && `&q=${encodeURIComponent(query)}`}${page && `&page=${page}`}`);

  // randomly sort the data to ensure a decent mix of types
  return data.collection.items;
}

export async function getAsset(assetId: string, mediaType: string): Promise<string> {
  const { data } = await axios.get(`${BASE_URL}/asset/${assetId}`);
  let extension: string;

  if (mediaType === "audio") extension = ".mp3";
  else if (mediaType === "image") extension = ".jpg";
  else if (mediaType === "video") extension = ".mp4";
  else {
    console.error("Invalid media type", { mediaType, assetId });
    throw new Error("Invalid media type");
  }
  
  const href = data.collection.items.filter((item: { href: string }) => item.href.includes(extension))[0].href;
  
  return href;
}

export async function getAssetMetadata(mediaType: string, assetId: string): Promise<AssetMetadata> {
  const { data } = await axios.get(`https://images-assets.nasa.gov/${mediaType}/${assetId}/metadata.json`);

  return data as AssetMetadata;
}
